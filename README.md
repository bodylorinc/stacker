# stacker

## A school project: Arduino binary calculator with a stack memory for operands and instructions. Minimal UI: 5 buttons and 2 byte LED display

You should put the `includes` folders .h files into separate folders with matching names under `Arduino/libraries`.

The circuit looks like this:
![](circuit.png)

The minimal UI was constrained by the lack of parts at hand.
The display control logic (in `OneDRaster.h`) cycles the decimal counter while switching the transistors to display the two bytes it keeps on the LEDS.

The lower LED line typically displays the binary number that is being entered. The upper LED line shows the cursor (the bit currently selected). The five buttons are debounced by the logic in `debPin.h`. The 2nd button flips the selected bit in the number that is entered, and advances the cursor. The 3th button advances the cursor (it wraps aroud at the end ot the byte). By default the entered number should show zero, and for example hitting the 3nd button twice and then the 2nd changes it to 0010 0000 (decimal 32).

The 4th and 5th buttons push the number on the input to the operator and the data stack respectively, interpeting it as a number or an opcode (see the table in `finiStack.h` for the planned opcodes and the core logic). The currently implemented opcodes are:


| opcode | binary | description |
| ------ | ------ | ------ |
|0	| 0000 0000 | pop, add, result to stack|
|1  	| 0000 000X | pop, sub, result to stack|
|2  	| 0000 00X0 | pop, mul, result to stack|
|3  	| 0000 00XX | pop, div, result to stack|
|4  	| 0000 0X00 | pop, mod, result to stack|
|5  	| 0000 0X0X | pop, and, result to stack|
|6  	| 0000 0XX0 | pop, or, result to stack |
...
|128	| X000 0000 | print stacks to serial|
...
|254	| XXXX XXX0 | empty full opStack |
|255	| XXXX XXXX | empty full dataStack|

The first button pops and executes the operation on the opStack, popping the requisite number of arguments from the DataStack. The result (if any) is displayed briefly on the upper LED line, and typically pushed back to the dataStack, making it easy to chain together calculations. Errors (stack full or pop from empty stack) are indicated by cycling the display.
