#ifndef DRASTER_H
#define DRASTER_H
#include "Arduino.h"
class OneDRaster{
  public:
    byte b1; 
    byte b2;
    OneDRaster( const int cloc, const int reset, const int p1, const int p2){
      
      clockPin = cloc;
      resetPin =  reset;
      pin1 = p1;
      pin2 = p2;
      }
    void setup(){
      pinMode(clockPin, OUTPUT);
      pinMode(resetPin, OUTPUT);
      pinMode(pin1, OUTPUT);
      pinMode(pin2, OUTPUT);
      }
    void reset(){
      digitalWrite(resetPin, HIGH);
      digitalWrite(resetPin, LOW);
      }
    void tick(){
      digitalWrite(clockPin, HIGH);
      digitalWrite(clockPin, LOW);
      }
    void cycle(int k, int t){
   
      bool which = LOW;
      for (int i=0;i<k;i++){
        reset();
        which =  !which;
        digitalWrite(pin1, which);
        digitalWrite(pin2, !which);
        for (int j=0; j<8; j++){
          delay(t);
          tick();        
         }
       }
    }
    void show(){
   	dworld(pin1, b1);
   	dworld(pin2, b2);	
   }
   void zero(){
   	digitalWrite(pin1, HIGH);
   	digitalWrite(pin2, HIGH);
   }
    
  private:
    int clockPin;
    int resetPin;
    int pin1;
    int pin2;
    int bits;
     
    void dworld(int pin, byte b){
    	reset();
    	for (int j=0; j<8; j++){
    	  bool val = bitRead(b, j);
    	  digitalWrite(pin, !val);
          delay(1);
          digitalWrite(pin, HIGH);
          tick();        
        }
    }
  };
#endif
