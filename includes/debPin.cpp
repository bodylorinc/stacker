#include "debPin.h"

// Outside of class
debPin *pointerToClass; // declare a pointer to testLib class

static void IH0(void) {
  pointerToClass->IH(0);
}
static void IH1(void) {
  pointerToClass->IH(1);
}
static void IH2(void) {
  pointerToClass->IH(2);
}
static void IH3(void) {
  pointerToClass->IH(3);
}
static void IH4(void) {
  pointerToClass->IH(4);
}
typedef void(*FunctionPointer)();
FunctionPointer ihs[5] = {IH0, IH1, IH2, IH3, IH4};

void debPin::begin(int n, const int pins[], unsigned long cooldowns_[]) {
  pointerToClass = this;
  for (int i=0;i<n;i++){
  	int ind = pins[i];
  	pinInd[i] = ind;
  	
  	pinMode(ind, INPUT);
  	attachInterrupt(digitalPinToInterrupt(ind), ihs[i], RISING);
	cooldowns[i] = cooldowns_[i];
	}
  
}

unsigned long debPin::sinceRisen(unsigned long t ,int pin){return (t - lastChanged[pin]);}

void debPin::IH(int pin) {
  byte now = digitalRead(pinInd[pin]);
  unsigned long t = millis();
  
  if (sinceRisen(t, pin) > cooldowns[pin]){
  	localPointerToCallback[pin](now);	
  	lastChanged[pin] = t;
  }
}
