#ifndef DENPIN_H
#define DEBPIN_H

#include "Arduino.h"

typedef void(*intFP)(int);
class debPin {

public:
  void begin(int n, const int interruptPins[], unsigned long cooldowns[]);
  void IH(int pin);
  void setCallback(void (*userDefinedCallback)(const int), int pin) {
                      localPointerToCallback[pin] = userDefinedCallback; }
  unsigned long sinceRisen(unsigned long t, int pin);
private:
  unsigned long lastChanged[5];
  unsigned long cooldowns[5]; 
  int pinInd[5];
  intFP localPointerToCallback[5];


} ;

#endif
