#ifndef FINISTACK_H
#define FINISTACK_H
#include "Arduino.h"

//A simple bounded stack. STL's version was causing weird compile errors
template <typename T, unsigned int N>
class FiniStack{
	public:
	  unsigned int size(){return n;}
	  bool overflowed = LOW;
	  bool underflowed = LOW;
	  void push(T thing){
	  	if (n<N){
	  		storage[n] = thing;
	  		n++;
	  			
	  	}
	  	else{ overflowed = HIGH;}
	  }
	  T pop(){
	  	if (has()){
	  		T x = storage[n-1];
	  		n--;
	  		return x;
	  		
	  	}
	  	else{ underflowed = HIGH; return 0;}
	  }
	  void dump(){
	  	for (int i=n;i>0;i--){
	  		Serial.print(n-i);
	  		Serial.print(": ");
	  		Serial.println(storage[i-1]);
	  	}
	  }
	  bool has(){
	  	return (n>0);
	  }
	  void trash(){
	  	n=0;
	  }
	  T peek(){
	  	if (has()){ return storage[n-1];}
	  	else{ underflowed = HIGH; return 0;}
	  }
	private:
	  unsigned int n=0;
	  T storage[N];
	
};


/*

The "documentation" of the planned opcodes and their effect. Only the first few (the elementary operations), #128 (stack dump to serial), #254 and #255 (empty the stacks) are implemented now.

The meaning of the five buttons:

Execute | Flip bit | Advance | Push to opStack | Push to dataStack

OPCODES:
0	| 0000 0000 | pop, add, result to stack
1  	| 0000 000X | pop, sub, result to stack
2  	| 0000 00X0 | pop, mul, result to stack
3  	| 0000 00XX | pop, div, result to stack
4  	| 0000 0X00 | pop, mod, result to stack
5  	| 0000 0X0X | pop, and, result to stack
6  	| 0000 0XX0 | pop, or, result to stack
7  	| 0000 0XXX | NOOP
#################################################
8  	| 0000 X000 | add (operand1 remains)
9  	| 0000 X00X | sub
10 	| 0000 X0X0 | mul
11 	| 0000 X0XX | div
12 	| 0000 XX00 | mod
13 	| 0000 XX0X | and 
14 	| 0000 XXX0 | or
15 	| 0000 XXXX | NOOP
#################################################
16	| 000X 0000 | not
17	| 000X 000X | mul 2
18	| 000X 00X0 | increment
19	| 000X 00XX | decrement
#################################################
...
#################################################
32	| 00X0 0000 | pop from stack
33	| 00X0 000X | switch top 2
34	| 00X0 00X0 | duplicate top
35	| 00X0 00XX | pop, push to opStack 
#################################################
...
################################################# The things below this instruction
64	| 0X00 0000 | pop from opStack
65	| 0X00 000X | switch top 2 on opStack
66	| 0X00 00X0 | duplicate top on opStack
67	| 0X00 00XX | pop, push to data stack 
#################################################
...
################################################x
128	| X000 0000 | print to serial
...
254	| XXXX XXX0 | empty full opStack
255	| XXXX XXXX | empty full dataStack



*/

/*The core logic of the stackmachine. It has to byte stacks, one for instructions and one for operands and can pop an instruction and execute it*/
typedef void(*v2vFP)(void);
template <unsigned int N>
class StackMachine{
   public:
	FiniStack<byte, N> * dat;
	FiniStack<byte, N> * op;
	v2vFP errorf;
      void setup(FiniStack<byte, N> * dat_, FiniStack<byte, N> * op_, v2vFP errorf_){
      	dat = dat_;
      	op = op_;
      	errorf = errorf_;
      
      }
      void serialDump(){
         Serial.println("");
	  Serial.print("dataStack: size = ");
	  Serial.println(dat->size());
	  dat->dump();
	  Serial.print("opStack: size = ");
	  Serial.println(op->size());
	  op->dump();
	}
      inline bool popBinary(byte remanent){
        
        if (dat->size() < 2){ return HIGH;}
        byte a = dat->pop();
        byte b = dat->pop();
        byte res = binary(remanent, a, b);
        dat->push(res);
        return LOW;
        
      }
      inline byte binary(byte remanent, byte a, byte b){
      	switch (remanent) {
		case 0: return a + b;
		case 1: return  a - b;
		case 2: return  a * b;
		case 3: return a / b;
		case 4: return a % b;
		case 5: return a & b;	
		case 6: return a | b;	
		}
      }
      bool execute(){
        bool fail = HIGH;
	if (op->has()){
		byte opc = op->pop();
		if (opc < 8){
			fail = popBinary(opc);
		} else if (opc < 16){
			fail=LOW;
		} else {
		switch (opc) {
		  case 128:
			serialDump();
			fail = LOW;
		  	break;
		  case 254:
			op->trash();
			fail = LOW;
		  	break;
		  case 255:
			dat->trash();
			fail = LOW;
		  	break;
 		  default:
 		  	fail=LOW;
		        break;
		}
		}
	}
	if (fail){
		errorf();
	}
	return fail;

}

};
#endif
