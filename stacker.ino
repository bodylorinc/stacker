#include "debPin.h"
#include "OneDRaster.h"
#include "finiStack.h"

debPin dePins; 
const int inpins[] = {0, 1, 2, 3, 7};
unsigned long cooldowns[] = {300, 300, 300, 300, 300};
OneDRaster disp( 10, 11, 12, 13);
//////////////////////////////////////////////////////////////////////
volatile int curs = 7;
volatile byte infield = 0;
volatile unsigned long last_reset = 0;
volatile unsigned long until_reset = 10000000;
/////////////////////////////////////////////////////////////////////
const int CS = 128;
FiniStack<byte, CS> dataStack;
FiniStack<byte, CS> opStack;
StackMachine<CS> machine;

void prep_reset(unsigned long k){
  last_reset = millis();
  until_reset = k;
}

void soft_reset(){
  disp.b1 = 0;
  disp.b2 = 0;
  curs = 7;
  bitSet(disp.b2, curs );
 }

void errorReset(){
  Serial.println("There was a Stack Error");
  disp.cycle(60, 200);  
  prep_reset(1000);
  infield = 0;
  }
void overPush( FiniStack<byte,CS>* stack, byte x){
  stack->push(x);
  if (stack->overflowed){
    stack->overflowed=LOW;
    errorReset();
    }
  }

void _flip(int n){ bitWrite(infield, n, !bitRead(infield, n)); }
  
void pushD(const int ps) {
  overPush(&dataStack, infield);
  disp.b2 = infield;
  prep_reset(1000);
  infield = 0; 
}
void pushO(const int ps) {
  overPush(&opStack, infield);
  disp.b2 = infield;
  prep_reset(1000);
  infield = 0;
 
}
void advance(const int ps){
 
  curs += -1;
  if (curs < 0){curs = 7;}
  disp.b2 = 0;
  bitSet(disp.b2, curs );
}
void flip(const int ps){
 
  _flip(curs);
  advance(0);
  disp.b1 = infield;
}
void execute(const int ps) {
 // machine.serialDump();
  machine.execute();
 // machine.serialDump();
  if (dataStack.has()){
    byte top = dataStack.peek();
    disp.b2 = top;
    prep_reset(4000);  
  } else {prep_reset(1000);}
  
}


void setup() {
  Serial.begin(9600);
  while (! Serial);
  dePins.begin(5, inpins, cooldowns);
  dePins.setCallback(execute, 0);
  dePins.setCallback(flip, 1);
  dePins.setCallback(advance, 2);
  dePins.setCallback(pushO, 3);
  dePins.setCallback(pushD, 4);

  disp.setup();
  machine.setup(&dataStack, &opStack, errorReset);
  soft_reset();
  
}

void loop() {
  //disp.cycle(100, 100);
  if ((millis()-last_reset) > until_reset){
    soft_reset();
    prep_reset(10000000);
  }
  disp.show();
 
   
}
